# Portfolio

Questo progetto è il mio portfolio personale, sviluppato con Vue.js e Tailwind CSS.
Per quanto sia un progetto semplice, ho avuto l'opportunità di usare in modo approfondito questi due frameworks, in particolare Vue, che ho utilizzato con due librerie aggiuntive, [vue-router](https://router.vuejs.org/) per gestire le rotte e [vueuse](https://vueuse.org/), che fornisce hooks molto utili.
