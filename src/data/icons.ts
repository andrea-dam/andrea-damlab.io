import xBold from "@iconify-icons/ph/x-bold";
import codeBold from "@iconify-icons/ph/code-bold";
import computerDesktop from "@iconify-icons/zondicons/computer-desktop";
import arrowCircleUpRounded from "@iconify-icons/material-symbols/arrow-circle-up-rounded";
import sunBold from "@iconify-icons/ph/sun-bold";
import moonBold from "@iconify-icons/ph/moon-bold";
import spinnerIcon from "@iconify-icons/gg/spinner";
import envelopeSimple from "@iconify-icons/ph/envelope-simple";
import linkedinIcon from "@iconify-icons/mdi/linkedin";
import gitlabIcon from "@iconify-icons/fa6-brands/gitlab";
import resumeIcon from "@iconify-icons/mdi/resume";
import githubIcon from "@iconify-icons/mdi/github";
import fileTypeHtml from "@iconify-icons/vscode-icons/file-type-html";
import fileTypeCss from "@iconify-icons/vscode-icons/file-type-css";
import bootstrapIcon from "@iconify-icons/logos/bootstrap";
import fileTypeTailwind from "@iconify-icons/vscode-icons/file-type-tailwind";
import javascriptIcon from "@iconify-icons/logos/javascript";
import fileTypeVue from "@iconify-icons/vscode-icons/file-type-vue";
import fileTypePhp3 from "@iconify-icons/vscode-icons/file-type-php3";
import laravelIcon from "@iconify-icons/logos/laravel";
import fileTypeSql from "@iconify-icons/vscode-icons/file-type-sql";
import barsIcon from "@iconify-icons/uil/bars";
import toggleRightFill from "@iconify-icons/ph/toggle-right-fill";
import toggleLeft from "@iconify-icons/ph/toggle-left";
import reactIcon from "@iconify-icons/logos/react";

const icons = {
	xBold,
	codeBold,
	computerDesktop,
	arrowCircleUpRounded,
	sunBold,
	moonBold,
	spinnerIcon,
	envelopeSimple,
	linkedinIcon,
	gitlabIcon,
	resumeIcon,
	fileTypeHtml,
	fileTypeCss,
	bootstrapIcon,
	fileTypeTailwind,
	javascriptIcon,
	fileTypeVue,
	fileTypePhp3,
	laravelIcon,
	fileTypeSql,
	barsIcon,
	toggleRightFill,
	toggleLeft,
	reactIcon,
	githubIcon,
};

const projectIcons = {
	laravel: icons.laravelIcon,
	html: icons.fileTypeHtml,
	css: icons.fileTypeCss,
	javascript: icons.javascriptIcon,
	react: icons.reactIcon,
	vue: icons.fileTypeVue,
	tailwind: icons.fileTypeTailwind,
	bootstrap: icons.bootstrapIcon,
};

export { projectIcons, icons };
