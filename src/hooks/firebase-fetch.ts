import { collection, getDocs, type Firestore } from "firebase/firestore";
import { inject, onMounted, ref } from "vue";

export default function useFirebaseFetch<T>(docs: string) {
	const firestore = inject("firestore");
	const data = ref<T[] | null>(null);

	onMounted(async () => {
		const querySnapshot = await getDocs(collection(firestore as Firestore, docs));

		data.value = querySnapshot.docs.map(queryDocument => queryDocument.data() as T);
	});

	return { data };
}
