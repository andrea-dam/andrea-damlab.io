import { getStorage, ref as firebaseRef, getDownloadURL } from "firebase/storage";
import { computed, onMounted, ref } from "vue";

export default function useImageUrl(image: string) {
	const storage = getStorage();

	const isAFirebaseUrl = computed(() => image.charAt(0) === "g");

	const imageRef = firebaseRef(storage, isAFirebaseUrl.value ? image : "");
	const actualPath = ref();
	const isLoading = ref(false);
	
	onMounted(async () => {
		isLoading.value = true;
		if (isAFirebaseUrl.value) {
			const url = await getDownloadURL(imageRef);
			actualPath.value = url;
		} else {
			actualPath.value = image;
		}

		isLoading.value = false;
	});

	return { actualPath, isLoading };
}
