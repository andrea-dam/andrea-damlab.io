import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { createApp } from "vue";
import App from "./App.vue";
import { Icon } from "@iconify/vue";
import { router } from "./router.js";

import "./style.css";
import "animate.css";

import TheSpinner from "./components/ui/TheSpinner.vue";

const firebase = initializeApp({
	apiKey: import.meta.env.VITE_FIREBASE_API_KEY,
	authDomain: import.meta.env.VITE_FIREBASE_AUTH_DOMAIN,
	projectId: import.meta.env.VITE_FIREBASE_PROJECT_ID,
	storageBucket: import.meta.env.VITE_FIREBASE_STORAGE_BUCKET,
	messagingSenderId: import.meta.env.VITE_FIREBASE_MESSAGING_SENDER_ID,
	appId: import.meta.env.VITE_FIREBASE_APP_ID,
});

const firestore = getFirestore(firebase);

createApp(App).use(router).component("Icon", Icon).component("TheSpinner", TheSpinner).provide("firestore", firestore).mount("#app");
