import { createRouter, createWebHistory } from "vue-router";

import AllProjects from "./components/views/AllProjects.vue";
import AllCertificates from "./components/views/AllCertificates.vue";
import AboutMe from "./components/views/AboutMe.vue";
import NotFound from "./components/views/NotFound.vue";

const routes = [
	{ path: "/", redirect: "/projects" },
	{ path: "/projects", component: AllProjects },
	{ path: "/certificates", component: AllCertificates },
	{ path: "/about-me", component: AboutMe },
	{ path: "/:pathMatch(.*)*", component: NotFound },
];

const router = createRouter({
	history: createWebHistory(),
	routes,
});

export { router };
