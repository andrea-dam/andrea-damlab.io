interface Project {
	title: string;
	description: string;
	image: string;
	icons: ("laravel" | "html" | "css" | "javascript" | "react" | "vue" | "tailwind" | "bootstrap")[];
	code?: string;
	isOnline?: boolean;
	url?: string;
}

interface Certificate {
	name: string;
	image: string;
	entity: string;
	url?: string;
}

export type { Project, Certificate };
